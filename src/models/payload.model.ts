// export interface IPayload {

//     firstname: String,
//     name: String,
//     email: String,
//     userId: Number,
//     Type: String,

// };

import { Error400 } from "../errors/errors";

export class Payload {
    id_user: number
    email: string
    name: string
    first_name: string
    type: number
    type_name: string

    public constructor(init?: Partial<Payload>) {
        Object.assign(this, init);
    }
}