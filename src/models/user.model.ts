import { Error400, Error404, Error498 } from "../errors/errors"

export class User {
    id_user?: number
    email?: string
    password?: string
    name?: string
    first_name?: string
    suspended?: boolean
    type?: number
    type_name?: string
    token?: string
    refresh_token?: string

    public constructor(init?: Partial<User>) {
        Object.assign(this, init);
    }

    public IsIdUserValid(): boolean {
        if (this.id_user === undefined) {
            throw new Error400("Missing argument, 'id_user' can not be NULL")
        }
        if (this.id_user < 0) {
            throw new Error400("Out of range argument, 'id_user' can not be a negative number")
        }
        return true
    }

    public IsEmailValid(): boolean {
        if (this.email === undefined) {
            throw new Error400("Missing argument, 'email' can not be NULL")
        }
        if (this.email === "") {
            throw new Error400("Empty argument, 'email' can not be EMPTY")
        }
        return true
    }

    public IsPasswordValid(): boolean {
        if (this.password === undefined) {
            throw new Error400("Missing argument, 'password' can not be NULL")
        }
        if (this.password === "") {
            throw new Error400("Empty argument, 'password' can not be EMPTY")
        }
        return true
    }

    public IsNameValid(): boolean {
        if (this.name === undefined) {
            throw new Error400("Missing argument, 'name' can not be NULL")
        }
        if (this.name === "") {
            throw new Error400("Empty argument, 'name' can not be EMPTY")
        }
        return true
    }

    public IsFirstNameValid(): boolean {
        if (this.first_name === undefined) {
            throw new Error400("Missing argument, 'first_name' can not be NULL")
        }
        if (this.first_name === "") {
            throw new Error400("Empty argument, 'first_name' can not be EMPTY")
        }
        return true
    }

    public IsSuspendedValid(): boolean {
        if (this.suspended === undefined) {
            throw new Error400("Missing argument, 'suspended' can not be NULL")
        }
        return true
    }

    public IsTokenValid(): boolean {
        if (this.token === undefined) {
            throw new Error498("Missing argument, 'token' can not be NULL")
        }
        if (this.token === "") {
            throw new Error498("Empty argument, 'token' can not be EMPTY")
        }
        if (!this.token.startsWith('Bearer ')) {
            throw new Error498("Token Invalid")
        }
        return true
    }

    public IsRefreshTokenValid(): boolean {
        if (this.refresh_token === undefined) {
            throw new Error498("Missing argument, 'refresh_token' can not be NULL")
        }
        if (this.refresh_token === "") {
            throw new Error498("Empty argument, 'refresh_token' can not be EMPTY")
        }
        return true
    }

    public IsUserSuspended(): boolean {
        if (this.suspended) {
            throw new Error404('Suspended user, this user is supended and can not be use')
        }
        return false
    }
}

// Convert the result send by the database into a single user struct
export function convertQueryResToUser(query_res: any): User {
    const res_user: any = query_res.recordset[0]

    return new User({
        id_user: res_user.Id_Users,
        email: res_user.Email,
        name: res_user.Name,
        first_name: res_user.First_Name,
        suspended: res_user.Suspended,
        type_name: res_user.Type,
        type: res_user.Id_Types,
    })
}
