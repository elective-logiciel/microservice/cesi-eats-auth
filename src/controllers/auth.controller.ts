import { Router } from 'express';
import { AuthData } from '../datas/auth.data';
import { TokenData } from '../datas/token.data';
import { Error412, Error498 } from '../errors/errors';
import { User } from '../models/user.model';
import { Payload } from '../models/payload.model';
import { Log } from "../models/log.model"
import { Logger } from "../services/logging.service"

const config = require('../config')

const authRouter = Router();
const authData = new AuthData()
const tokenData = new TokenData()
const logSvc = new Logger()

const private_access_key: string = config.jwt.private_key;
const private_refresh_key: string = config.jwt.refresh_private_key;

authRouter.post('/sign_in', async function(req, res, next) {
    try {
        console.log('Request send:', req.originalUrl)

        const user: User = new User ({
            email: req.body.email,
            password: req.body.password 
        })

        user.IsEmailValid()
        user.IsPasswordValid()

        var getUser: User = await authData.LoginUser(user);

        getUser.IsUserSuspended()

        var payload:Payload = new Payload({
            id_user: getUser.id_user,
            email: getUser.email,
            name: getUser.name,
            first_name: getUser.first_name,
            type: getUser.type,
            type_name: getUser.type_name,
        })
        
        if (private_access_key === undefined || private_access_key === "") {
            throw new Error412("Environnement Invalide, private access key is invalid.")
        }

        if (private_refresh_key === undefined || private_refresh_key === "") {
            throw new Error412("Environnement Invalide, private refresh key is invalid.")
        }

        var access_token: string = await tokenData.GetAccessToken(payload, private_access_key)
        var refresh_token: string = await tokenData.GetRefreshToken(payload, private_refresh_key)

        var logSuccess: Log = new Log({
            status_name: "Created",
            status: 201,
            message: "User successfully sign in",
            service_name: config.service_name,
        })
        logSvc.info(logSuccess)

        res.cookie("refresh_token", refresh_token).status(201).json({
            status: 201,
            name: 'Created',
            message: 'Sign in',
            payload: payload, 
            access_token: access_token,
            refresh_token: refresh_token, 
            expire: 86400
        });

    } catch (err) {
        next(err);
    }

});

authRouter.post('/refresh', async function (req, res, next) {
    try {
        console.log('Request send:', req.originalUrl)

        var user: User = new User({
            id_user: req.body.id_user,
            token: req.headers.authorization,
            refresh_token: req.body.refresh_token,
        })
    
        user.IsIdUserValid()
        user.IsTokenValid()
        user.IsRefreshTokenValid()
    
        user.token = String(user.token).replace("Bearer ", "");
    
        await tokenData.VerifyAccessToken(user).catch((err) => { throw err });
        await tokenData.VerifyRefreshToken(user).catch((err) => { throw err });

        await tokenData.RevokeAccessToken(user).catch((err) => { throw err });
        await tokenData.RevokeRefreshToken(user).catch((err) => { throw err });
    
        var logUser: User = await authData.RefreshUser(user);

        var payload: Payload = new Payload({
            id_user: logUser.id_user,
            email: logUser.email,
            name: logUser.name,
            first_name: logUser.first_name,
            type: logUser.type,
            type_name: logUser.type_name,
        })
        
        if (private_access_key === undefined || private_access_key === "") {
            throw new Error412("Environnement Invalide, private access key is invalid.")
        }

        if (private_refresh_key === undefined || private_refresh_key === "") {
            throw new Error412("Environnement Invalide, private refresh key is invalid.")
        }
    
        var newAcessToken = await tokenData.GetAccessToken(payload, private_access_key).catch((err) => { throw err });
        var newRefreshToken = await tokenData.GetRefreshToken(payload, private_refresh_key).catch((err) => { throw err });
    
        var logSuccess: Log = new Log({
            status_name: "Ok",
            status: 200,
            message: "User token successfully refresh",
            service_name: config.service_name,
        })
        logSvc.info(logSuccess)

        res.cookie("refresh_token", newRefreshToken).status(200).json({
            status: 200,
            name: 'OK',
            message: 'Refresh',
            payload: payload,
            access_token: newAcessToken,
            refresh_token: newRefreshToken, expire: 86400
        });

    } catch(err) {
        next(err)
    }
});

authRouter.get('/verify',async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        var user: User = new User({
            id_user: req.body.id_user,
            token: req.headers.authorization
        })

        user.IsIdUserValid()
        user.IsTokenValid()

        user.token = String(user.token).replace("Bearer ", "");

        await tokenData.VerifyAccessToken(user).catch((err) => { throw err });

        var logSuccess: Log = new Log({
            status_name: "Ok",
            status: 200,
            message: "User token valid",
            service_name: config.service_name,
        })
        logSvc.info(logSuccess)

        res.status(200).json({
            status: 200,
            name: 'OK',
            message: 'Valid token'
        })
        
    } catch (err) {
        next(err)
    }
})

authRouter.get('/verify_refresh', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        var user: User = new User({
            id_user: req.body.id_user,
            refresh_token: req.headers.authorization
        })

        user.IsIdUserValid()
        user.IsRefreshTokenValid()

        user.refresh_token = String(user.refresh_token).replace("Bearer ", "");

        await tokenData.VerifyRefreshToken(user).catch((err) => { throw err });

        var logSuccess: Log = new Log({
            status_name: "Ok",
            status: 200,
            message: "User refresh token valid",
            service_name: config.service_name,
        })
        logSvc.info(logSuccess)

        res.status(200).json({
            status: 200,
            name: 'OK',
            message: 'Valid refresh token'
        })

    } catch (err) {
        next(err)
    }
})

authRouter.post('/sign_out', async function (req, res, next) {
    try {
        console.log('Request send:', req.originalUrl)

        var user: User = new User ({
            id_user: req.body.id_user,
            token: req.headers.authorization
        })

        user.IsIdUserValid()
        user.IsTokenValid()

        user.token = String(user.token).replace("Bearer ", "");

        await tokenData.SignOut(user).catch((err) => { throw err });

        var logSuccess: Log = new Log({
            status_name: "Ok",
            status: 200,
            message: "User token successfully log out",
            service_name: config.service_name,
        })
        logSvc.info(logSuccess)

        res.clearCookie("refresh_token").status(200).json({
            status: 200,
            name: 'OK',
            message: "Sign Out"
        });

    } catch (err) {
        next(err);
    }
    
});

export {authRouter};
