import jwt, { JwtPayload, sign, SignOptions } from 'jsonwebtoken'
// import { mssql } from '../data/mssql.data';
// import { getDataConfig, getJWTConfig } from '../config'
// import { IPayload } from '../interfaces/payload.interface';
import { Payload } from '../models/payload.model';
// import { Int, VarChar } from 'mssql';
import { Error404, Error412, Error498 } from '../errors/errors';
import { loadOptions } from '@babel/core';

const config = require('../config')

// const MssqlDB = new mssql();
// const dataConfig = getDataConfig();

export class JWT {

    public async SignJwt(payload: Payload, private_key: string, expire_time: string): Promise<string> {

        // read private key value
        // const privateKey = private_key;
        
        if (private_key === undefined || private_key === "")  {
            throw new Error412("Environnement Invalide, private key is not valid")
        }

        const signInOptions: SignOptions = {
            // RS256 uses a public/private key pair. The API provides the private key
            // to generate the JWT. The client gets a public key to validate the
            // signature
            algorithm: 'HS512',
            expiresIn: expire_time,
            issuer: 'CesiEats:Insider:issuer',
            audience: 'EatsApp'
        };

        var token = sign({
            id_user: payload.id_user,
            email: payload.email,
            name: payload.name,
            first_name: payload.first_name,
            type: payload.type,
            type_name: payload.type_name
        }, private_key, signInOptions);
        
        // generate JWT
        return token;
    }

    // public async refreshToken(payload: IPayload, private_key: string): Promise<string> {

    //     var expire_time: string = '7d';

    //     var refreshToken = await this.signJwt(payload, private_key, expire_time).catch((err) => { throw err });

    //     // A mettre dans le microservice (envoyer userId et token par requete post)
    //     const pool = await MssqlDB.get(dataConfig.cesiEatsApp.name, dataConfig.cesiEatsApp.config);

    //     var request = pool.request();

    //     request.input('id_users', Int, payload.userId);
    //     request.input('token', VarChar(500), refreshToken);

    //     await request.query('UPDATE Users SET Refresh_Token = @token WHERE Id_Users = @id_users');

    //     pool.close();

    //     return refreshToken;
    // }

    // public async accessToken(payload: IPayload, private_key: string): Promise<string> {

    //     var expire_time: string = '1d';

    //     var acessToken = await this.signJwt(payload, private_key, expire_time);
        
    //     // A mettre dans le microservice (envoyer userId et token par requete post)
    //     const pool = await MssqlDB.get(dataConfig.cesiEatsApp.name, dataConfig.cesiEatsApp.config);
        
    //     var request = pool.request();
        
    //     request.input('id_users', Int, payload.userId);
    //     request.input('token', VarChar(500), acessToken);
        
    //     await request.query('UPDATE Users SET Token = @token WHERE Id_Users = @id_users');

    //     pool.close();

    //     return acessToken;
    // }

    public CheckJwt(token, privateKey): JwtPayload | string {
            
        const payload = jwt.verify(token, privateKey);

        if (typeof payload === 'string') {
            throw new Error404('Payload is not valid')
        }

        return payload;
    }

    // public async verifyAccessToken(token: string, id_user: Number): Promise<string | jwt.JwtPayload> {

    //     const pool = await MssqlDB.get(dataConfig.cesiEatsApp.name, dataConfig.cesiEatsApp.config);

    //     var request = pool.request();

    //     request.input('token', VarChar(500), token);
    //     request.input('id_user', Int, id_user);

    //     var exist = (await request.query('SELECT COUNT(*) as count FROM Users WHERE Token = @token AND Id_Users = @id_user')).recordset[0].count;

    //     pool.close();

    //     if (exist == 0) {
    //         throw new Error498('Invalid Token')
    //     }

    //     const privateAccessKey = getJWTConfig.private_key;

    //     if (privateAccessKey == null) {
    //         throw new Error412("Environnement Invalide")
    //     }

    //     var payload: string | jwt.JwtPayload = this.checkJWT(token, privateAccessKey);
            
    //     return payload;

    // }

    // public async verifyRefreshToken(token: string, id_user: Number): Promise<string | jwt.JwtPayload> {

    //     const pool = await MssqlDB.get(dataConfig.cesiEatsApp.name, dataConfig.cesiEatsApp.config);

    //     var request = pool.request();

    //     request.input('token', VarChar(500), token);
    //     request.input('id_user', Int, id_user);

    //     var exist = (await request.query('SELECT COUNT(*) FROM Users WHERE Refresh_Token = @token AND Id_Users = @id_user')).recordset[0].count;

    //     pool.close();

    //     if (exist == 0) {
    //         throw new Error498('Invalid Token')
    //     }

    //     const privateRefreshKey = getJWTConfig.refresh_private_key;

    //     if (privateRefreshKey == null) {
    //         throw new Error412("Environnement Invalide")
    //     }

    //     var payload: string | jwt.JwtPayload = this.checkJWT(token, privateRefreshKey);

    //     return payload;

    // }

    // public async revokeRefreshToken(refreshToken: string, id_user: Number) {
    //     // A mettre dans le microservice (envoyer userId et token par requete post)
    //     const pool = await MssqlDB.get(dataConfig.cesiEatsApp.name, dataConfig.cesiEatsApp.config);

    //     var request = pool.request();

    //     request.input('refresh_token', VarChar(500), refreshToken);
    //     request.input('id_user', Int, id_user);

    //     var revoke = await request.query('UPDATE Users SET Refresh_Token = NULL WHERE Refresh_Token = @refresh_token AND Id_Users = @id_user');

    //     if (revoke.rowsAffected[0] == 0) {
    //         throw new Error404("Unable to revoke");
    //     }

    //     pool.close();

    // }

    // public async revokeAcessToken(AccessToken: string, id_user: Number) {
    //     // A mettre dans le microservice (envoyer userId et token par requete post)
    //     const pool = await MssqlDB.get(dataConfig.cesiEatsApp.name, dataConfig.cesiEatsApp.config);

    //     var request = pool.request();

    //     request.input('token', VarChar(500), AccessToken);
    //     request.input('id_user', Int, id_user);

    //     var revoke = await request.query('UPDATE Users SET Token = NULL WHERE Token = @token AND Id_Users = @id_user');

    //     if (revoke.rowsAffected[0] == 0) {
    //         throw new Error404("Unable to revoke");
    //     }

    //     pool.close();

    // }

    // public async signOut(idUser: Number, access_token:string) {
    //     // A mettre dans le microservice (envoyer userId et token par requete post)
    //     const pool = await MssqlDB.get(dataConfig.cesiEatsApp.name, dataConfig.cesiEatsApp.config);

    //     var request = pool.request();

    //     request.input('idUser', Int, idUser);
    //     request.input('token', VarChar(500), access_token);

    //     var userExists = await request.query('SELECT * FROM Users WHERE Id_Users = @idUser AND Token = @token');

    //     if (userExists.rowsAffected[0] == 0) {
    //         throw new Error404("Unauthorized"); 
    //     }
        
    //     await request.query('UPDATE Users SET Token = NULL, Refresh_Token = NULL WHERE Id_Users = @idUser AND Token = @token');
        
    //     pool.close();
    // }
}