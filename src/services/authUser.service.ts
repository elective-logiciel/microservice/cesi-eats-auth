// import { mssql } from '../data/mssql.data';
// import { getDataConfig } from '../config'
// import { IUser } from '../interfaces/user.interace';
// import { IPayload } from '~/interfaces/payload.interface';
// import { Int, VarChar } from "mssql";
// import { Error404 } from '../errors/errors';

// const MssqlDB = new mssql();
// const dataConfig = getDataConfig();

// export class User {

//     public async  LogUser(email: string, password: string): Promise<IUser> {

//         const pool = await MssqlDB.get(dataConfig.cesiEatsApp.name, dataConfig.cesiEatsApp.config);

//         var request = pool.request()

//         request.input('email', VarChar(50), email);
//         request.input('password', VarChar(50), password);

//         var user = await request.query('SELECT Id_Users, Email, Types.[Type], Name, First_Name FROM Users LEFT JOIN Types ON Users.Id_Types = Types.Id_Types WHERE Email = @email AND Password = @password');
    
//         if (user.rowsAffected[0] == 0) {
//             throw new Error404('Unknown User');
//         }

//         pool.close();

//         return user.recordset[0];
//     }

//     public async RefreshUser(id_user: Number) {
//         const pool = await MssqlDB.get(dataConfig.cesiEatsApp.name, dataConfig.cesiEatsApp.config);

//         var request = pool.request()

//         request.input('id_user', Int, id_user)

//         var user = await request.query('SELECT Id_Users, Email, Types.[Type], Name, First_Name FROM Users LEFT JOIN Types ON Users.Id_Types = Types.Id_Types WHERE Id_Users = @id_user');

//         if (user.rowsAffected[0] == 0) {
//             throw new Error404('Unknown User');
//         }
//         pool.close();

//         return user.recordset[0];
//     }

//     public createPayload(Name, First_Name, Email, Id_Users, Type) {
//         var payload: IPayload = {
//             name: Name,
//             firstname: First_Name,
//             email: Email,
//             userId: Id_Users,
//             Type: Type
//         }

//         return payload;
//     }

// }