import jwt, { JwtPayload } from 'jsonwebtoken'
import { mssql, sqlConfig } from "../services/mssql.service"
import { User } from "../models/user.model";
import { Payload } from "../models/payload.model";
import { Error404, Error498, Error412 } from "../errors/errors";
import { JWT } from "../services/jwt.service";
import { Int, VarChar } from 'mssql';


const sql = require('mssql')
const config = require('../config')

const mssqlDB = new mssql();
const jwtSvc = new JWT();

export class TokenData {
    public async GetRefreshToken(payload: Payload, private_key: string): Promise<string> {

        var expire_time: string = '7d';

        var refreshToken = await jwtSvc.SignJwt(payload, private_key, expire_time);
        
        const pool = await await mssqlDB.get(sqlConfig.name, sqlConfig.config)

        var request = pool.request()

        request.input('id_users', Int, payload.id_user)
        request.input('token', VarChar(500), refreshToken)

        await request.query('UPDATE Users SET Refresh_Token = @token WHERE Id_Users = @id_users');

        pool.close();

        return refreshToken;
    }

    public async GetAccessToken(payload: Payload, private_key: string): Promise<string> {
        var expire_time: string = '1d';

        var accessToken = await jwtSvc.SignJwt(payload, private_key, expire_time);

        const pool = await await mssqlDB.get(sqlConfig.name, sqlConfig.config)

        var request = pool.request()

        request.input('id_users', Int, payload.id_user);
        request.input('token', VarChar(500), accessToken);        

        await request.query('UPDATE Users SET Token = @token WHERE Id_Users = @id_users');

        pool.close();
        

        return accessToken;
    }

    public async VerifyAccessToken(user: User) {

        const pool = await await mssqlDB.get(sqlConfig.name, sqlConfig.config)

        var request = pool.request()

        request.input('id_user', Int, user.id_user);
        request.input('token', VarChar(500), user.token);

        var res = await request.query('SELECT Id_Users FROM Users WHERE Token = @token AND Id_Users = @id_user');

        pool.close();

        if (res.rowsAffected[0] == 0) {
            throw new Error498('Invalid Token, the token does not match the one in the token in the data base.')
        }

        if (config.jwt.private_key === undefined || config.jwt.private_key === "") {
            throw new Error412("Environnement Invalid, private key is invalid.")
        }

        var payload: string | jwt.JwtPayload = jwtSvc.CheckJwt(user.token, config.jwt.private_key);

        return payload
    }

    public async VerifyRefreshToken(user: User) {
        const pool = await await mssqlDB.get(sqlConfig.name, sqlConfig.config)

        var request = pool.request();

        request.input('id_user', Int, user.id_user);
        request.input('token', VarChar(500), user.refresh_token);

        var res = await request.query('SELECT Id_Users FROM Users WHERE Refresh_Token = @token AND Id_Users = @id_user')

        pool.close();

        if (res.rowsAffected[0] == 0) {
            throw new Error498('Invalid Refresh Token, the refresh token does not match the one in the token in the data base.')
        }

        if (config.jwt.refresh_private_key === undefined || config.jwt.refresh_private_key === "") {
            throw new Error412("Environnement Invalid, private refresh key is invalid.")
        }
        
        var payload: string | jwt.JwtPayload = jwtSvc.CheckJwt(user.refresh_token, config.jwt.refresh_private_key);

        return payload
    }

    public async RevokeAccessToken(user: User) {

        const pool = await await mssqlDB.get(sqlConfig.name, sqlConfig.config)

        var request = pool.request();

        request.input('id_user', Int, user.id_user);
        request.input('token', VarChar(500), user.token);

        var res = await request.query('UPDATE Users SET Token = NULL WHERE Token = @token AND Id_Users = @id_user');

        if (res.rowsAffected[0] == 0) {
            throw new Error404("Unable to revoke token");
        }

        pool.close();
    }

    public async RevokeRefreshToken(user: User) {
        
        const pool = await await mssqlDB.get(sqlConfig.name, sqlConfig.config)

        var request = pool.request();

        request.input('id_user', Int, user.id_user);
        request.input('token', VarChar(500), user.refresh_token);

        var res = await request.query('UPDATE Users SET Refresh_Token = NULL WHERE Refresh_Token = @token AND Id_Users = @id_user');

        if (res.rowsAffected[0] == 0) {
            throw new Error404("Unable to revoke refresh token");
        }

        pool.close();
    }

    public async SignOut(user: User) {
        const pool = await await mssqlDB.get(sqlConfig.name, sqlConfig.config)

        var request = pool.request();

        request.input('id_user', Int, user.id_user);
        request.input('token', VarChar(500), user.token);

        var userExists = await request.query('SELECT * FROM Users WHERE Id_Users = @id_user AND Token = @token');

        if (userExists.rowsAffected[0] == 0) {
            throw new Error404("Unauthorized");
        }

        await request.query('UPDATE Users SET Token = NULL, Refresh_Token = NULL WHERE Id_Users = @id_user AND Token = @token');

        pool.close();
    }
}