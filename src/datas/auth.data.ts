import { mssql, sqlConfig } from "../services/mssql.service"
import { User } from "../models/user.model";
import { Error404, Error403 } from "../errors/errors";
import { convertQueryResToUser } from "../models/user.model";

const sql = require('mssql')

const mssqlDB = new mssql();

export class AuthData {
    public async LoginUser(user: User): Promise<User> {
        const pool = await await mssqlDB.get(sqlConfig.name, sqlConfig.config)

        var request = pool.request()

        request.input('email', sql.NVarChar, user.email)
        request.input('password', sql.NVarChar, user.password)

        const res = await request.query('SELECT Id_Users, Email, Types.[Type], Name, First_Name, Suspended, Types.[Id_Types] FROM Users LEFT JOIN Types ON Users.Id_Types = Types.Id_Types WHERE Email = @email AND Password = @password');

        pool.close();

        if (res.rowsAffected[0] == 0) {
            throw new Error403('Unknown User, wrong email or password');
        }

        return convertQueryResToUser(res);
    }

    public async RefreshUser(user: User): Promise<User> {
        const pool = await await mssqlDB.get(sqlConfig.name, sqlConfig.config)

        var request = pool.request()

        request.input('id_user', sql.Int, user.id_user)

        const res = await request.query('SELECT Id_Users, Email, Types.[Type], Name, First_Name, Suspended, Types.[Id_Types] FROM Users LEFT JOIN Types ON Users.Id_Types = Types.Id_Types WHERE Id_Users = @id_user');

        pool.close();

        if (res.rowsAffected[0] == 0) {
            throw new Error404('Unknown User, this id does not return a user.');
        }

        return convertQueryResToUser(res);
    }
}