const assert = require("assert");
const dotenv = require("dotenv");

// read in the .env file
dotenv.config();

// capture the environment variables the application needs
const { 
    PORT,
    HOST,
    SQL_SERVER,
    SQL_DATABASE,
    SQL_USER,
    SQL_PASSWORD,
    SQL_POOL_NAME,
    SQL_DRIVER,
    SQL_ENCRYPT,
    ACCESS_TOKEN_PRIVATE_KEY,
    ACCESS_TOKEN_PUBLIC_KEY,
    REFRESH_TOKEN_PRIVATE_KEY,
    REFRESH_TOKEN_PUBLIC_KEY,
    LOG_SERVER_ADDRESS,
    LOG_SERVER_PORT,
    MICROSERVICE_NAME,
} = process.env;

// validate the required configuration information
assert(PORT, "PORT configuration is required.");
assert(HOST, "HOST configuration is required.");
assert(SQL_SERVER, "SQL_SERVER configuration is required.");
assert(SQL_DATABASE, "SQL_DATABASE configuration is required.");
assert(SQL_USER, "SQL_USER configuration is required.");
assert(SQL_PASSWORD, "SQL_PASSWORD configuration is required.");
assert(SQL_POOL_NAME, "SQL_EATS_NAME configuration is required.");
assert(SQL_DRIVER, "SQL_DRIVER configuration is required.");
assert(SQL_ENCRYPT, "SQL_ENCRYPT configuration is required.");
assert(ACCESS_TOKEN_PRIVATE_KEY, "ACCESS_TOKEN_PRIVATE_KEY configuration is required.");
assert(ACCESS_TOKEN_PUBLIC_KEY, "ACCESS_TOKEN_PUBLIC_KEY configuration is required.");
assert(REFRESH_TOKEN_PRIVATE_KEY, "REFRESH_TOKEN_PRIVATE_KEY configuration is required.");
assert(REFRESH_TOKEN_PUBLIC_KEY, "REFRESH_TOKEN_PUBLIC_KEY configuration is required.");
assert(LOG_SERVER_PORT, "LOG_SERVER_PORT configuration is required.");
assert(LOG_SERVER_ADDRESS, "LOG_SERVER_PORT configuration is required.");
assert(MICROSERVICE_NAME, "MICROSERVICE_NAME configuration is required.");

// export the configuration information
module.exports = {
    service_name: MICROSERVICE_NAME,
    port: PORT,
    host: HOST,
    sql: {
        server: SQL_SERVER,
        database: SQL_DATABASE,
        user: SQL_USER,
        password: SQL_PASSWORD,
        dirver: SQL_DRIVER,
        pool_name: SQL_POOL_NAME,
        options: {
            encrypt: SQL_ENCRYPT
        }
    },
    jwt: {
        private_key: ACCESS_TOKEN_PRIVATE_KEY,
        public_key: ACCESS_TOKEN_PUBLIC_KEY,
        refresh_private_key: REFRESH_TOKEN_PRIVATE_KEY,
        refresh_public_key: REFRESH_TOKEN_PUBLIC_KEY,
    },
    log: {
        address: LOG_SERVER_ADDRESS,
        port: LOG_SERVER_PORT,
    }
};