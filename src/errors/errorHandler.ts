import { Log } from "../models/log.model"
import { Logger } from "../services/logging.service"
const config = require('../config')

const logSvc = new Logger()

function logError(err, req, res, next) {
    console.error(err)
    if (err.response) {
        err = err.response.data

        const logErr: Log = new Log({
            status: err.statusCode || 500,
            status_name: err.name,
            message: 'Responce err: ' + err.message,
            service_name: config.service_name,
        })
        logSvc.error(logErr)

    } else if (err.request) {
        err = err.request.data

        const logErr: Log = new Log({
            status: err.statusCode || 500,
            status_name: err.name,
            message: 'Request err: ' + err.message,
            service_name: config.service_name,
        })
        logSvc.error(logErr)

    } else {
        const logErr: Log = new Log({
            status: err.statusCode || 500,
            status_name: err.name,
            message: err.message,
            service_name: config.service_name,
        })
        logSvc.error(logErr)
    }

    next(err)
}

function returnError(err, req, res, next) {
    if (err.response) {
        err = err.response.data

        res.status(err.statusCode || 500).json({
            name: err.name,
            status: err.statusCode || 500,
            message: 'Responce err: ' + err.message
        })

    } else if (err.request) {
        err = err.request.data

        res.status(err.statusCode || 500).json({
            name: err.name,
            status: err.statusCode || 500,
            message: 'Request err: ' + err.message
        })

    } else {
        res.status(err.statusCode || 500).json({
            name: err.name,
            status: err.statusCode || 500,
            message: err.message
        })
    }
}

export { logError, returnError }