import { Error400 } from "../../src/errors/errors"
import { User } from "../../src/models/user.model";

const valid_user: User = new User({
    id_user: 1,
    email: 'email',
    password: 'password',
    name: 'name',
    first_name: 'first_name',
    suspended: true
})

const unvalid_user: User = new User({
    id_user: -1,
    email: '',
    password: '',
    name: '',
    first_name: '',
    suspended: true
})

const undefined_user: User = new User({})

describe('User structure field are correct', () => {
    test('Id User is set and valid', () => {
        const user: User = valid_user

        expect(user.IsIdUserValid()).toBeTruthy()
    })

    test('Email is set and valid', () => {
        const user: User = valid_user

        expect((user.IsEmailValid())).toBeTruthy()
    })

    test('Password is set and valid', () => {
        const user: User = valid_user

        expect((user.IsPasswordValid())).toBeTruthy()
    })


    test('Name is set and valid', () => {
        const user: User = valid_user

        expect(user.IsNameValid()).toBeTruthy()
    })


    test('First name is set and valid', () => {
        const user: User = valid_user

        expect(user.IsFirstNameValid()).toBeTruthy()
    })

    test('Suspended is set and valid', () => {
        const user: User = valid_user

        expect(user.IsSuspendedValid()).toBeTruthy
    })
})

describe('User structure field are undefined', () => {
    test('Id User is set and unvalid', () => {
        const user: User = unvalid_user
        const err: Error400 = new Error400("Out of range argument, 'id_user' can not be a negative number")

        function caughtError() {
            user.IsIdUserValid()
        }

        expect(caughtError).toThrowError(err)
    })

    test('Email is set and unvalid', () => {
        const user: User = unvalid_user
        const err: Error400 = new Error400("Empty argument, 'email' can not be EMPTY")

        function caughtError() {
            user.IsEmailValid()
        }

        expect(caughtError).toThrow(err)
    })

    test('Password is set and unvalid', () => {
        const user: User = unvalid_user
        const err: Error400 = new Error400("Empty argument, 'password' can not be EMPTY")

        function caughtError() {
            user.IsPasswordValid()
        }

        expect(caughtError).toThrow(err)
    })

    test('Name is set and unvalid', () => {
        const user: User = unvalid_user
        const err: Error400 = new Error400("Empty argument, 'name' can not be EMPTY")

        function caughtError() {
            user.IsNameValid()
        }

        expect(caughtError).toThrow(err)
    })

    test('First name is set and unvalid', () => {
        const user: User = unvalid_user
        const err: Error400 = new Error400("Empty argument, 'first_name' can not be EMPTY")

        function caughtError() {
            user.IsFirstNameValid()
        }

        expect(caughtError).toThrow(err)
    })
})

describe('User structure field are undefined', () => {
    test('Id User is not set and undefined', () => {
        const user: User = undefined_user
        const err: Error400 = new Error400("Missing argument, 'id_user' can not be NULL")

        function caughtError() {
            user.IsIdUserValid()
        }

        expect(caughtError).toThrow(err)
    })

    test('Email is not set and undefined', () => {
        const user: User = undefined_user
        const err: Error400 = new Error400("Missing argument, 'email' can not be NULL")

        function caughtError() {
            user.IsEmailValid()
        }

        expect(caughtError).toThrow(err)
    })

    test('Password is not set and undefined', () => {
        const user: User = undefined_user
        const err: Error400 = new Error400("Missing argument, 'password' can not be NULL")

        function caughtError() {
            user.IsPasswordValid()
        }

        expect(caughtError).toThrow(err)
    })

    test('Name is set and unvalid', () => {
        const user: User = unvalid_user
        const err: Error400 = new Error400("Empty argument, 'name' can not be EMPTY")

        function caughtError() {
            user.IsNameValid()
        }

        expect(caughtError).toThrow(err)
    })

    test('First name is not set and undefined', () => {
        const user: User = undefined_user
        const err: Error400 = new Error400("Missing argument, 'first_name' can not be NULL")

        function caughtError() {
            user.IsFirstNameValid()
        }

        expect(caughtError).toThrow(err)
    })

    test('Suspended is not set and undefined', () => {
        const user: User = undefined_user
        const err: Error400 = new Error400("Missing argument, 'suspended' can not be NULL")

        function caughtError() {
            user.IsSuspendedValid()
        }

        expect(caughtError).toThrow(err)
    })
})
