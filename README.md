# Cesi Eats auth

This is the microservice for managing auth.

This microservice can be access on ```http://51.103.90.32:3001/auth``` and ```http://51.103.90.183:3001/auth```.

## Routes

### auth

#### ```/auth/(:id_user)```

Get auth data by his id user.

type: get

Input: 

- id_user: int, user's id

Output:

- 

